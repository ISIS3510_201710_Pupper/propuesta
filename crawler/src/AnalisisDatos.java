import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import opennlp.tools.chunker.ChunkerME;
import opennlp.tools.chunker.ChunkerModel;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;

public abstract class AnalisisDatos {

	public static String[] tokenizar(String input){
		InputStream modelIn = null;
		try {
			modelIn = new FileInputStream("models/en-token.bin");
			TokenizerModel model = new TokenizerModel(modelIn);
			Tokenizer tokenizer = new TokenizerME(model);
			String tokens[] = tokenizer.tokenize(input);
			return tokens;
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				}
				catch (IOException e) {
				}
			}
		}
		return null;
	}

	public static String[] createTags(String[] input){
		InputStream modelIn = null;

		try {
			modelIn = new FileInputStream("models/en-pos-maxent.bin");
			POSModel model = new POSModel(modelIn);
			POSTaggerME tagger = new POSTaggerME(model);
			String tags[] = tagger.tag(input);
			return tags;
		}
		catch (IOException e) {
			// Model loading failed, handle the error
			e.printStackTrace();
		}
		finally {
			if (modelIn != null) {
				try {
					modelIn.close();
				}
				catch (IOException e) {
				}
			}
		}
		return input;
	}

	public static void calcularFrecuencia(String parrafo, String[] tokens, String[] tags){
		Map<String,Long> freqs = new HashMap<String,Long>();
		for (int i = 0;i <tokens.length;i++) {
			if(tags[i].startsWith("J") || tags[i].startsWith("N") || tags[i].startsWith("V")){
				if (freqs.containsKey(tokens[i])) {
					freqs.put(tokens[i], freqs.get(tokens[i]) + 1);
				} else {
					freqs.put(tokens[i], new Long(1));
				}
			}
		}
		PrintWriter writer = null;
		try{
			writer = new PrintWriter("resultados/freqs.csv");
			for (Map.Entry<String, Long> entry : freqs.entrySet())
			{

				writer.println(entry.getKey() + "," + entry.getValue());
			}

			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}



	}

	//	public static void chunks(String[] sent, String[] pos){
	//
	//		InputStream modelIn = null;
	//		ChunkerModel model = null;
	//
	//		try {
	//			modelIn = new FileInputStream("models/en-chunker.bin");
	//			model = new ChunkerModel(modelIn);
	//			ChunkerME chunker = new ChunkerME(model);
	//			String tags[] = chunker.chunk(sent, pos);
	//			for (String tag : tags) {
	//				System.out.println(tag);
	//			}
	//		} catch (IOException e) {
	//			// Model loading failed, handle the error
	//			e.printStackTrace();
	//		} finally {
	//			if (modelIn != null) {
	//				try {
	//					modelIn.close();
	//				} catch (IOException e) {
	//				}
	//			}
	//		}
	//	}
}


