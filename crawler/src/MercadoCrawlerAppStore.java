import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MercadoCrawlerAppStore {
	
	public final static String URL_PRINCIPAL = "http://www.apple.com/itunes/charts/paid-apps/";

	//Atributos
	
	public final static String SEL_NOMBRE = "[itemprop='name']";
	
	
	// Metodo para obtener las referencias filtradas a partir del URL
	public static HashSet<String> getHrefs() throws Exception{
		Document doc = Jsoup.connect(URL_PRINCIPAL).timeout(0).get();
		
		HashSet<String> hrefs = new HashSet<String>();
		
		Elements anchors = doc.getElementsByClass("section-content");
		
		for (Element element : anchors) {
			
			Elements anchors2 = element.getElementsByTag("h3");
			System.out.println(anchors2.isEmpty());
			
			for (Element element2 : anchors2) {
				
				Elements anchors3 = element2.getElementsByTag("a");
				for (Element element3 : anchors3) {
					
					hrefs.add(element3.attr("href").toString());
				}
			}
		}
		
		return hrefs;
	}
	
	// Metodo que extrae el texto de los aspectos revelantes del app
	public static void selectAttrs(HashSet<String> hrefs) throws Exception{
		for (String url : hrefs) {
			Document detailDocument = Jsoup.connect(url).timeout(0).get();
			System.out.println(detailDocument.select(SEL_NOMBRE).text());
		}
	}
	
	//Main
	public static void main(String[] args){
		try {
			HashSet<String> hrefs = getHrefs();
			selectAttrs(hrefs);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
