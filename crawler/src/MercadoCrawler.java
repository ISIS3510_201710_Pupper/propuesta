import java.io.IOException;
import java.util.HashSet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class MercadoCrawler {

	public final static int C_ANDROID = 1;
	public final static int C_IOS = 2;

	public final static String URL_PREFIX_ANDROID = "https://play.google.com/";
	public final static String URL_PRINCIPAL_ANDROID = "https://play.google.com/store/apps/category/FINANCE/collection/topselling_free";

	public final static String URL_PREFIX_IOS = "http://appshopper.com";
	public final static String URL_PRINCIPAL_IOS = "http://appshopper.com/search/?cat=all&platform=all&device=all&search=dog+walk&searchdev=all&sort=rel&dir=asc&page=";

	//Atributos para Android
	public final static String A_SEL_NOMBRE = ".id-app-title";
	public final static String A_SEL_NUMERO_RATINGS = ".rating-count";
	public final static String A_SEL_RATING = ".score";
	public final static String A_SEL_DESCRIPCION = "[itemprop='description']";
	public final static String A_SEL_CAMBIOS = ".recent-change";
	public final static String A_SEL_RATING4 = ".rating-bar-container five";
	public final static String A_SEL_RATING5 = ".rating-bar-container four";

	//Atributos para iOS
	public final static String I_SEL_NOMBRE = ".app-detail-header";
	public final static String I_SEL_DESCRIPCION = ".description-text";
	public final static String I_SEL_CAMBIOS = "#mr_appExpand";
	public final static String I_SEL_RATING = ".store-rating-stars";


	// Metodo para obtener las referencias filtradas a partir del URL
	public static HashSet<String> getHrefs(int cPlataforma) throws Exception{
		HashSet<String> hrefs = new HashSet<String>();

		if(cPlataforma==C_ANDROID){
			Document doc = Jsoup.connect(URL_PRINCIPAL_ANDROID).timeout(0).get();
			Elements anchors = doc.getElementsByClass("card-click-target");
			for (Element element : anchors) {
				hrefs.add(URL_PREFIX_ANDROID + element.attr("href").toString()); 
			}
		}

		else{
			//			Document doc = Jsoup.connect(URL_PRINCIPAL_IOS).timeout(0).get();
			//			Elements anchors = doc.getElementsByClass("name").select("a");
			//			anchors.remove(0);
			//			for (Element element : anchors) {
			//				hrefs.add(URL_PREFIX_IOS + element.attr("href").toString());
			//			}
			for(int i = 3; i<5;i++){
				System.out.println(URL_PRINCIPAL_IOS + i);
				Document doc = Jsoup.connect(URL_PRINCIPAL_IOS + i).timeout(0).get();
				Elements anchors = doc.getElementsByClass("section app").select(".slide-wrap").select("a");
				anchors.remove(0);
				for (Element element : anchors) {
					if(!element.attr("href").toString().contains("login")){
						hrefs.add(URL_PREFIX_IOS + element.attr("href").toString());
					}
				}
			}
		}
		return hrefs;
	}

	// Metodo que extrae el texto de los aspectos revelantes del app
	public static String selectAttrs(HashSet<String> hrefs, int cPlataforma) throws Exception{

		StringBuilder sb = new StringBuilder();
		if(cPlataforma==C_ANDROID){
			for (String url : hrefs) {
				Document detailDocument = Jsoup.connect(url).timeout(0).get();
				sb.append(detailDocument.select(A_SEL_NUMERO_RATINGS).text());
				sb.append(detailDocument.select(A_SEL_NOMBRE).text());
				sb.append(detailDocument.select(A_SEL_NOMBRE).text());
				sb.append(detailDocument.select(A_SEL_NOMBRE).text());
				sb.append(detailDocument.select(A_SEL_NOMBRE).text());
			}
		}
		else{
			for (String url : hrefs) {
				if(!url.contains("%")){
					Document detailDocument = Jsoup.connect(url).timeout(0).get();
					int appRating = ratingiOS(detailDocument.select(I_SEL_RATING));
					if(appRating >=4){
						sb.append(detailDocument.select(I_SEL_NOMBRE).select("h1").text() + ", ");
						System.out.println(detailDocument.select(I_SEL_NOMBRE).select("h1").text());
						sb.append(detailDocument.select(I_SEL_DESCRIPCION).text() + ", ");
						System.out.println(detailDocument.select(I_SEL_DESCRIPCION).text());
						sb.append(detailDocument.select(I_SEL_CAMBIOS).next().next().next().text()+ ", ");
						System.out.println(detailDocument.select(I_SEL_CAMBIOS).next().next().next().text());
						
					}
				}
			}
		}
		return sb.toString();
	}

	//Metodo para calcular rating the apps iOS
	public static int ratingiOS(Elements elements){
		Elements listaUL = elements.select("ul").select("li");
		for (Element element : listaUL) {
			if(!element.getElementsByClass("selected").isEmpty()){
				return Integer.parseInt(element.attr("data-rating"));
			}
		}
		return 0;
	}



	//Main
	public static void main(String[] args){
		try {

			HashSet<String> hrefs = getHrefs(C_IOS);
			String output = selectAttrs(hrefs,C_IOS);
			String[] tokens = AnalisisDatos.tokenizar(output);
			String[] tags = AnalisisDatos.createTags(tokens);
			AnalisisDatos.calcularFrecuencia(output, tokens, tags);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
